# SatNOGS WebGUI Docker Image

GitLab Repository: https://gitlab.com/librespacefoundation/satnogs/docker-satnogs-webgui

This is the repository for Docker Images of [SatNOGS WebGUI](https://gitlab.com/librespacefoundation/satnogs/satnogs-webgui/).
SatNOGS WebGUI provides a browser-based interface for setup and configuration of SatNOGS Ground Stations.
It is based on [RaspAP-webgui](https://github.com/RaspAP/raspap-webgui).

## Starting the app

For simplicity in usage the docker-satnogs-webgui employs the use of the Docker Compose tool to build the containers needed for the app.
Two images are used by the app, one runs the lighttpd server and the other the php fast-cgi process manager. The php container also has
some helper applications that are needed like NetworkManager and SoapySDR tools.

To build and start the containers you can run:

`docker compose up`

### Host machine connections

For the application to function correctly 2 connections must be already be created to the host machine.
You can create these connections with

`nmcli connection add type wifi ifname wlan0 con-name Hotspot autoconnect yes ssid raspi-webgui wifi.mode ap wifi.band bg wifi-sec.key-mgmt wpa-psk wifi-sec.psk "ChangeMe" ipv4.method shared ipv4.addresses 10.42.0.1/24 802-11-wireless.channel 1`

`nmcli connection add type bridge con-name bridge0 ifname br0 autoconnect no`

### Host machine policykit

For the application to function correctly there must be added some rules with policykit. Those rules gives permissions to the user from inside
the container to manage Network Manager connections.
You can create the necessary rules with

`echo "[Network Manager www-data allow modify settings, enable/disable networking]
Identity=unix-user:www-data
Action=org.freedesktop.NetworkManager.settings.modify.system;org.freedesktop.NetworkManager.network-control;org.freedesktop.NetworkManager.wifi.scan;org.freedesktop.NetworkManager.wifi.share.open;org.freedesktop.NetworkManager.wifi.share.protected;org.freedesktop.NetworkManager.enable-disable-statistics
ResultAny=yes" > 10-network-manager.pkla`

# License

GNU AGPLv3
